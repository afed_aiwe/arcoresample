package com.example.ar_stabilization.handlers

import java.lang.Exception

class IncorrectRotateException(errorMessage: String, cause: Throwable) : Exception(errorMessage, cause) {

}