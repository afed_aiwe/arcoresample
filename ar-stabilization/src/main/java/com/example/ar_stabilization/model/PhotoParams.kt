package com.example.ar_stabilization.model

import android.graphics.PointF
import android.util.Size

class PhotoParams(var distance: Float, var yaw: Float, var positionObject: PointF, var sizeOfImage: Size) {
}