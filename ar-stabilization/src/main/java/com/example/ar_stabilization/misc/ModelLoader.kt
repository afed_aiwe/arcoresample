package com.example.ar_stabilization.misc

import android.content.Context
import android.graphics.Color
import android.os.Build
import com.google.ar.sceneform.math.Vector3
import com.google.ar.sceneform.rendering.Material
import com.google.ar.sceneform.rendering.MaterialFactory
import com.google.ar.sceneform.rendering.ModelRenderable
import com.google.ar.sceneform.rendering.ShapeFactory
import java.lang.ref.WeakReference
import java.util.concurrent.CompletableFuture

class ModelLoader(owner: ModelLoaderCallbacks) {
    companion object {
        private const val TAG = "ModelLoader"
    }

    private var owner: WeakReference<ModelLoaderCallbacks> = WeakReference(owner)
    private var future: CompletableFuture<Material>? = null

    /**
     * Starts loading the model specified. The result of the loading is returned asynchrounously via
     * [ModelLoaderCallbacks.setRenderable] or [ ][ModelLoaderCallbacks.onLoadException] (Throwable)}.
     *
     * @param resourceId the resource id of the .sfb to load.
     * @return true if loading was initiated.
     */
    open fun loadModel(context: Context?): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            future = MaterialFactory.makeOpaqueWithColor(context, com.google.ar.sceneform.rendering.Color(Color.RED))
            future?.thenAccept {material ->
                setRenderable(ShapeFactory.makeSphere(0.05f, Vector3(0f, 0f, 0f), material))
                }
            future?.exceptionally {
                onException(it)
            }
        }
        return future != null
    }

    fun onException(throwable: Throwable?): Material? {
        val listener = owner.get()
        listener?.onLoadException(throwable)
        return null
    }

    fun setRenderable(modelRenderable: ModelRenderable?): ModelRenderable? {
        val listener = owner.get()
        listener?.setRenderable(modelRenderable)
        return modelRenderable
    }

    /** Callbacks for handling the loading results.  */
    interface ModelLoaderCallbacks {
        fun setRenderable(modelRenderable: ModelRenderable?)
        fun onLoadException(throwable: Throwable?)
    }
}