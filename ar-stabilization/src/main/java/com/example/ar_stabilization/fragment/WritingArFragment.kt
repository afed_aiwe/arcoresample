package com.example.ar_stabilization.fragment

import android.Manifest
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import com.google.ar.core.Session
import com.google.ar.sceneform.ux.ArFragment

class WritingArFragment : ArFragment() {

    override fun getAdditionalPermissions(): Array<String?> {
        val additionalPermissions: Array<String> = super.getAdditionalPermissions()
        val permissionLength = if (additionalPermissions.isNullOrEmpty()) 0 else additionalPermissions.size
        val permissions = arrayOfNulls<String>(permissionLength + 1)
        permissions[0] = Manifest.permission.WRITE_EXTERNAL_STORAGE
        if (permissionLength > 0) {
            System.arraycopy(additionalPermissions, 0,permissions, 1, additionalPermissions.size)
        }
        return permissions
    }

    fun hasWritePermission(): Boolean =
        ActivityCompat.checkSelfPermission(this.requireActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED

    override fun getSessionFeatures(): MutableSet<Session.Feature> {
        return mutableSetOf(Session.Feature.SHARED_CAMERA)
    }
}