package com.example.ar_stabilization.extensions

import android.graphics.PointF
import android.graphics.Rect
import android.media.Image
import android.util.Size
import com.google.ar.core.CameraConfigFilter
import com.google.ar.core.Pose
import com.google.ar.core.Session
import com.google.ar.sceneform.math.Quaternion
import kotlin.math.*


fun Session.setupHighestImageResolution(): Size {
    var resolutionWidth = 0
    var resolutionHeight = 0
    val cameraConfigFilter = CameraConfigFilter(this)
    val cameraConfigs = getSupportedCameraConfigs(cameraConfigFilter)
    if (cameraConfigs.isEmpty()) {
        return Size(resolutionWidth, resolutionHeight)
    }
    pause()
    var highestResolution = 0
    var indexForHighestResolution = 0
    cameraConfigs.forEachIndexed { index, config ->
        val size = config.imageSize
        val resolution = size.width * size.height
        if (resolution > highestResolution) {
            highestResolution = resolution
            indexForHighestResolution = index
            resolutionWidth = size.width
            resolutionHeight = size.height
        }
    }
    this.cameraConfig = cameraConfigs[indexForHighestResolution]
    resume()
    return Size(resolutionWidth, resolutionHeight)
}

fun Image.YUV420TONV21(): ByteArray {
    val cameraPlaneY = planes[0].buffer
    val cameraPlaneU = planes[2].buffer
    val cameraPlaneV = planes[1].buffer

    val compositeByteArray = ByteArray(cameraPlaneY.capacity() + cameraPlaneU.capacity() + cameraPlaneV.capacity())

    cameraPlaneY.get(compositeByteArray, 0, cameraPlaneY.capacity())
    cameraPlaneU.get(compositeByteArray, cameraPlaneY.capacity(), cameraPlaneU.capacity())
    cameraPlaneV.get(compositeByteArray, cameraPlaneY.capacity() + cameraPlaneU.capacity(), cameraPlaneV.capacity())

    return compositeByteArray
}

fun Pose.calculateDistance(otherPose: Pose): FloatArray {
    val distanceArray = FloatArray(4)
    distanceArray[0] = tx() - otherPose.tx()
    distanceArray[1] = ty() - otherPose.ty()
    distanceArray[2] = tz() - otherPose.tz()
    distanceArray[3] = sqrt(distanceArray[0] * distanceArray[0] + distanceArray[1] * distanceArray[1] + distanceArray[2] * distanceArray[2])
    return distanceArray
}

//https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
fun Quaternion.toEulerAngles(): FloatArray {
    val eulerAngles = FloatArray(3)
    //roll (x-axis rotation)
    val sinr_cosp = 2 * (w + x + y * z)
    val cosr_cosp = 1 - 2 * (x * x + y * y)
    eulerAngles[0] = atan2(sinr_cosp, cosr_cosp)
    //pitch (y-axis rotation
    val sinp = 2 * (w * y - z * x)
    if (abs(sinp) < 1) {
        eulerAngles[1] = asin(sinp)
    } else {
        eulerAngles[1] = (Math.PI / 2).withSign(sinp.toDouble()).toFloat()
    }
    //yaw (z-axis rotation)
    val siny_cosp = 2 * (w *z + x * y)
    val cosy_cosp = 1 - 2 * (y * y + z * z)
    eulerAngles[2] = atan2(siny_cosp, cosy_cosp)
    return eulerAngles
}

fun Rect.scale(scaleX: Float, scaleY: Float) {
    val newWidth = width() * scaleX
    val newHeight = height() * scaleY
    val deltaX = (width() - newWidth) / 2
    val deltaY = (height() - newHeight) / 2
    set((left + deltaX).toInt(), (top + deltaY).toInt(), (right - deltaX).toInt(), (bottom - deltaY).toInt())
}
fun Rect.insetBy(translationX: Float, translationY: Float) {
    inset(abs(translationX).toInt(), abs(translationY).toInt())
}

fun PointF.screenPointToCameraPoint(screenSize: Size,
                                    cameraSize: Size
) {
    val newX = x - ((screenSize.width - cameraSize.width) / 2f)
    val newY = y - ((screenSize.height - cameraSize.height) / 2f)
    set(newX, newY)
}