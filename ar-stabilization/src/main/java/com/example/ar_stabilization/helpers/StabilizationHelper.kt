package com.example.ar_stabilization.helpers

import android.graphics.*
import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import android.os.Message
import android.util.Size
import com.example.ar_stabilization.extensions.*
import com.example.ar_stabilization.handlers.IncorrectRotateException
import com.example.ar_stabilization.model.PhotoParams
import com.google.ar.core.Pose
import com.google.ar.sceneform.AnchorNode
import com.google.ar.sceneform.ArSceneView
import com.google.ar.sceneform.Camera
import com.google.ar.sceneform.math.Quaternion
import java.io.File
import java.lang.IllegalArgumentException
import kotlin.math.*

class StabilizationHelper private constructor() {

    private var mapOfPhotoParams = mutableMapOf<String, PhotoParams>()

    private var mapOfTransformParams = mutableMapOf<String, TransformParams>()

    private var incorrectRotateListeners = mutableSetOf<IncorrectRotateListener>()

    private var supportingCenterPoint: PointF = PointF(0f,0f)

    companion object {
        private const val TAG = "StabilizationHelper"
        private const val EXTENSION_IMAGE = ".jpg"
        private const val STABILIZATION_POSTFIX = "_stabilization"
        private const val TRANSFORMATION_POSTFIX = "_transformation"
        private const val HANDLER_SIMPLE_THREAD_NAME = "BackgroundHandler"

        @Volatile
        private var INSTANCE: StabilizationHelper? = null

        @Synchronized
        fun getInstance(): StabilizationHelper = INSTANCE
            ?: StabilizationHelper()
                .also { INSTANCE = it }
    }

    fun getSupportingPositionForStabilization(
        arSceneView: ArSceneView,
        cameraSize: Size
    ): Boolean {
        val screenSize = Size(arSceneView.width, arSceneView.height)
        val centerPositionX = screenSize.width / 2f
        val centerPositionY = screenSize.height / 2f
        supportingCenterPoint.set(centerPositionX, centerPositionY)
        supportingCenterPoint.screenPointToCameraPoint(screenSize, cameraSize)
        return supportingCenterPoint.x != 0f && supportingCenterPoint.y != 0f
    }

    fun stabilizePhoto(folderPath: String) {
        val handlerThread = HandlerThread(HANDLER_SIMPLE_THREAD_NAME)
        handlerThread.start()
        val handler = Handler(handlerThread.looper)
        handler.post {
            val sizeOfCropRect = findMinCropSize()
            for((k, v) in mapOfPhotoParams) {
                val transformParams = mapOfTransformParams[k]
                if (transformParams != null) {
                    val inputFileName = "$folderPath/$k$EXTENSION_IMAGE"
                    val transformationFileName =
                        "$folderPath/$k$TRANSFORMATION_POSTFIX$EXTENSION_IMAGE"
                    val outputFileName = "$folderPath/$k$STABILIZATION_POSTFIX$EXTENSION_IMAGE"
                    val inputFile = File(inputFileName)
                    val tmpFile = File(transformationFileName)
                    val outputFile = File(outputFileName)
                    val transformedBitmap = getTransformedBitmap(inputFile, transformParams)
                    PhotoHelper.getInstance().saveImage(transformedBitmap, tmpFile)
                    try {
                        val croppedBitmap = getCroppedBitmap(transformedBitmap, sizeOfCropRect)
                        if (croppedBitmap != null) {
                            PhotoHelper.getInstance().saveImage(croppedBitmap, outputFile)
                        }
                    } catch (ex: IncorrectRotateException) {
                        incorrectRotateListeners.forEach {
                            it.onIncorrectRotate(ex.message ?: "Unknown error")
                        }
                    }
                }
            }
        }
        handlerThread.quitSafely()
    }

    private fun findMinCropSize(): Size {
        var minWidth = Float.MAX_VALUE
        var minHeight = Float.MAX_VALUE
        for((k, v) in mapOfPhotoParams) {
            val transformedRect = Rect(0, 0, v.sizeOfImage.width, v.sizeOfImage.height)
            val scale = mapOfTransformParams[k]?.scale ?: 1f
            val translationX = mapOfTransformParams[k]?.translationX ?: 0f
            val translationY = mapOfTransformParams[k]?.translationY ?: 0f
            val rotation = mapOfTransformParams[k]?.rotationRadians ?: 0f
            transformedRect.scale(scale, scale)
            transformedRect.insetBy(translationX, translationY)
            val cropRect = calculateLargestCropRect(transformedRect.width(), transformedRect.height(), rotation)
            minWidth = if (cropRect.width() < minWidth) cropRect.width() else minWidth
            minHeight = if (cropRect.height() < minHeight) cropRect.height() else minHeight
        }
        return Size(minWidth.toInt(), minHeight.toInt())
    }

    private fun getTransformedBitmap(inputFile: File, transformParams: TransformParams): Bitmap {
        val scale = transformParams.scale
        val (translationX, translationY) = transformParams.translationX to transformParams.translationY
        val rotationDegrees = Math.toDegrees(transformParams.rotationRadians.toDouble()).toFloat()
        val options = BitmapFactory.Options()
        val originalBitmap = BitmapFactory.decodeFile(inputFile.absolutePath, options)
        val transformedBitmap = Bitmap.createBitmap(originalBitmap.width, originalBitmap.height, Bitmap.Config.ARGB_8888)
        val transformationMatrix = Matrix()
        val canvas = Canvas(transformedBitmap)
        transformationMatrix.preScale(scale, scale, originalBitmap.width / 2f, originalBitmap.height / 2f)
        transformationMatrix.postTranslate(translationX, translationY)
        transformationMatrix.postRotate(-rotationDegrees, originalBitmap.width / 2f,originalBitmap.height / 2f)
        val paint = Paint()
        paint.isFilterBitmap = true
        canvas.drawBitmap(originalBitmap, transformationMatrix, paint)
        originalBitmap.recycle()
        return transformedBitmap
    }

    @Throws(IncorrectRotateException::class)
    private fun getCroppedBitmap(transformedBitmap: Bitmap, sizeOfCropRect: Size ): Bitmap? {
        var croppedBitmap: Bitmap? = null
        try {
            val halfWidthRect = sizeOfCropRect.width / 2
            val halfHeightRect = sizeOfCropRect.height / 2
            val halfWidthBitmap = transformedBitmap.width / 2
            val halfHeightBitmap = transformedBitmap.height / 2
            val top = halfHeightBitmap - halfHeightRect
            val left = halfWidthBitmap - halfWidthRect
            croppedBitmap =  Bitmap.createBitmap(
                transformedBitmap,
                left,
                top,
                sizeOfCropRect.width,
                sizeOfCropRect.height
            )
        } catch (ex: IllegalArgumentException) {
            if (sizeOfCropRect.width < sizeOfCropRect.height) {
                throw IncorrectRotateException("Very large angle", ex)
            }
        }
        return croppedBitmap
    }

    private fun calculateParamsForTransformation(photoName: String, photoParams: PhotoParams) {
        val supportingParams = getSupportingParamsForPhoto()
        if (supportingParams != null) {
            val scale = photoParams.distance / supportingParams.distance
            val translationX = (supportingCenterPoint.x - photoParams.positionObject.x) * scale
            val translationY = (supportingCenterPoint.y - photoParams.positionObject.y) * scale
            val rotationRadians = photoParams.yaw
            mapOfTransformParams[photoName] = TransformParams(scale, translationX, translationY, rotationRadians)
        }
    }

    fun calculateParamsForCurrentFrame(objectPose: Pose,
                                       arSceneView: ArSceneView,
                                       positionObject: PointF,
                                       photoName: String,
                                       cameraSize: Size) {
        val cameraObject = arSceneView.arFrame?.camera
        val cameraPose = cameraObject?.pose
        var distance = 0f
        if (cameraPose != null) {
            distance = calculateDistance(cameraPose, objectPose)
        }
        var yaw = 0f
        if (cameraObject != null) {
            yaw = calculateAngle(cameraObject)
        }
        val photoParams = PhotoParams(distance, yaw, positionObject, cameraSize)
        mapOfPhotoParams[photoName] = photoParams
        calculateParamsForTransformation(photoName, photoParams)
    }

    private fun calculateDistance(cameraPose: Pose, objectPose: Pose): Float {
        val distanceArray = objectPose.calculateDistance(cameraPose)
        return distanceArray[3]
    }

    private fun calculateAngle(cameraObject: com.google.ar.core.Camera): Float {
        val cameraRotation = cameraObject.displayOrientedPose.rotationQuaternion
        val eulerAngles = Quaternion(
            cameraRotation[0],
            cameraRotation[1],
            cameraRotation[2],
            cameraRotation[3]
        ).toEulerAngles()
        return eulerAngles[2]
    }

    fun getCurrentObjectPosition(cameraScene: Camera, currentAnchorNode: AnchorNode?): PointF {
        val worldPoint = currentAnchorNode?.worldPosition
        val currentPositionPoint = cameraScene.worldToScreenPoint(worldPoint)
        return PointF(currentPositionPoint.x, currentPositionPoint.y)
    }

    //https://gist.github.com/DavidBoyes/5290376
    //transformRectWidth - width transformation rect (not rotate)
    //transformRectHeight - height transformation rect (not rotate)
    private fun calculateLargestCropRect(transformedRectWidth: Int, transformedRectHeight: Int, angle: Float): RectF {
//        normalization angle
        val quadrant = floor(angle / Math.PI / 2.0).toInt() and 3
        val signAlpha = if ((quadrant and 1) == 0) angle.toDouble() else Math.PI - angle
        val alpha = ((signAlpha % Math.PI) + Math.PI) % Math.PI

        val width = transformedRectWidth * cos(alpha) + transformedRectHeight * sin(alpha)
        val height = transformedRectWidth * sin(alpha) + transformedRectHeight * cos(alpha)

        val gamma = if (transformedRectWidth < transformedRectHeight) atan2(width, height) else atan2(height, width)

        val delta = Math.PI - alpha - gamma

        val length = if (transformedRectWidth < transformedRectHeight) transformedRectHeight else transformedRectWidth

        val d = length * cos(alpha)
        val a = d * sin(alpha) / sin(delta)
        val y = a * cos(gamma)
        val x = y * tan(gamma)
        val right = (width - 2 * x) + x
        val bottom = (height - 2 * y) + y

        return RectF(
            x.toFloat(),
            y.toFloat(),
            right.toFloat(),
            bottom.toFloat()
        )

    }

    //https://stackoverflow.com/a/16778797
//    private fun calculateLargestCropRect(transformedRectWidth: Int, transformedRectHeight: Int, angle: Float): SizeF {
//        val widthIsLonger = transformedRectWidth >= transformedRectHeight
//        val sideLong: Int
//        val sideShort: Int
//        if (widthIsLonger) {
//            sideLong = transformedRectWidth
//            sideShort = transformedRectHeight
//        } else {
//            sideLong = transformedRectHeight
//            sideShort = transformedRectWidth
//        }
//        val sinA = abs(sin(angle))
//        val cosA = abs(cos(angle))
//        val wr: Float
//        val hr: Float
//        val x: Float
//        if ((sideShort <= (2f * sinA * cosA * sideLong)) || (abs(sinA - cosA) < 1e-10)) {
//            x = sideShort * 0.5f
//            if (widthIsLonger) {
//                wr = x / sinA
//                hr = x / cosA
//            } else {
//                wr = x / cosA
//                hr = x / sinA
//            }
//        } else {
//            val cos2A = (cosA * cosA) - (sinA * sinA)
//            wr = ((transformedRectWidth * cosA) - (transformedRectHeight * sinA)) / cos2A
//            hr = ((transformedRectHeight * cosA) - (transformedRectWidth * sinA)) / cos2A
//        }
//        return SizeF(wr, hr)
//    }

    //TODO: made private method after debugging
    fun getSupportingParamsForPhoto(): PhotoParams? {
        return mapOfPhotoParams.values.toTypedArray()[0]
    }

    //TODO: delete method after debugging
    fun getCurrentFrameParams(photoName: String): PhotoParams? {
        return mapOfPhotoParams[photoName]
    }

    //TODO: delete method after debugging
    fun getSupportingCenterPosition(): PointF {
        return supportingCenterPoint
    }

    fun addIncorrectRotateListener(listener: IncorrectRotateListener) {
        incorrectRotateListeners.add(listener)
    }

    fun removeIncorrectRotateListener(listener: IncorrectRotateListener) {
        incorrectRotateListeners.remove(listener)
    }

    class TransformParams(var scale: Float, var translationX: Float, var translationY: Float, var rotationRadians: Float)

    interface IncorrectRotateListener {
        fun onIncorrectRotate(errorMessage: String)
    }
}