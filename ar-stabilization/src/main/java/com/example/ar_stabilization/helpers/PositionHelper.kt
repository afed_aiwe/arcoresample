package com.example.ar_stabilization.helpers

import android.graphics.PointF
import android.util.Size
import com.example.ar_stabilization.extensions.screenPointToCameraPoint
import com.google.ar.sceneform.AnchorNode
import com.google.ar.sceneform.ArSceneView

class PositionHelper {
    companion object {
        @Volatile
        private var INSTANCE: PositionHelper? = null

        @Synchronized
        fun getInstance(): PositionHelper = INSTANCE
            ?: PositionHelper()
                .also { INSTANCE = it }
    }

    private val incorrectPositionListeners = mutableSetOf<IncorrectPositionListener>()

    fun checkPositionObject(arSceneView: ArSceneView, currentAnchorNode: AnchorNode?, cameraSize: Size) {
        val cameraScene = arSceneView.scene.camera
        val positionObject = StabilizationHelper.getInstance().getCurrentObjectPosition(cameraScene, currentAnchorNode)
        positionObject.screenPointToCameraPoint(
            Size(arSceneView.width, arSceneView.height),
            cameraSize
        )
        if (positionObject.x >= 0 && positionObject.y >= 0) {
            incorrectPositionListeners.forEach {
                it.onCorrectPosition(positionObject)
            }
        } else {
            incorrectPositionListeners.forEach {
                it.onIncorrectPosition(positionObject)
            }
        }
    }

    fun addIncorrectPositionListener(listener: IncorrectPositionListener) {
        incorrectPositionListeners.add(listener)
    }

    fun removeIncorrectPositionListener(listener: IncorrectPositionListener) {
        incorrectPositionListeners.remove(listener)
    }

    interface IncorrectPositionListener {
        fun onIncorrectPosition(objectPosition: PointF)
        fun onCorrectPosition(objectPosition: PointF)
    }
}