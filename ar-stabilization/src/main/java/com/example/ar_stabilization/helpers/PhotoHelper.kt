package com.example.ar_stabilization.helpers

import android.graphics.*
import android.os.*
import android.util.Log
import android.util.Size
import android.view.PixelCopy
import com.example.ar_stabilization.imageconverters.ImageToBitmapConverter
import com.example.ar_stabilization.imageconverters.Nv21ToJpegConverter
import com.google.ar.sceneform.ArSceneView
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.lang.Exception
import java.lang.reflect.InvocationTargetException

class PhotoHelper private constructor() {

    companion object {
        private const val TAG = "TakePhotoHelper"

        private const val HANDLER_SNAPSHOT_THREAD_NAME = "PixelCopier"
        private const val HANDLER_SIMPLE_THREAD_NAME = "BackgroundHandler"

        @Volatile
        private var INSTANCE: PhotoHelper? = null

        @Synchronized
        fun getInstance(): PhotoHelper = INSTANCE
            ?: PhotoHelper()
                .also { INSTANCE = it }
    }

    private fun createSimpleImage(arSceneView: ArSceneView, imageFile: File, callback: (() -> Unit)? = null) {
        try {
            val cameraImage = arSceneView.arFrame?.acquireCameraImage()
            if (cameraImage != null) {
                val handlerThread = HandlerThread(HANDLER_SIMPLE_THREAD_NAME)
                handlerThread.start()
                val handler = Handler(handlerThread.looper)
                handler.post {
                    val imageConverter: ImageToBitmapConverter = Nv21ToJpegConverter()
                    val bitmap = imageConverter.convert(cameraImage)
                    saveImage(bitmap, imageFile)
                    cameraImage.close()
                    Handler(Looper.getMainLooper()).post {
                        callback?.invoke()
                    }
                }
                handlerThread.quitSafely()
            }
        } catch (ex: InvocationTargetException) {
            ex.cause?.printStackTrace()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun takePhoto(arSceneView: ArSceneView, simpleImageFile: File, callback: (() -> Unit)? = null,  snapshotImageFile: File? = null) {
        if (snapshotImageFile != null) {
            createSnapshotImage(arSceneView, snapshotImageFile)
        }
        createSimpleImage(arSceneView, simpleImageFile, callback)
    }

    private fun createSnapshotImage(arSceneView: ArSceneView, snapshotImageFile: File) {
        val width = arSceneView.rootView.width
        val height = arSceneView.rootView.height
        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val handlerThread = HandlerThread(HANDLER_SNAPSHOT_THREAD_NAME)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            handlerThread.start()
            PixelCopy.request(arSceneView, bitmap, {copyResult ->
                if (copyResult == PixelCopy.SUCCESS) {
                    try {
                        saveImage(bitmap, snapshotImageFile)
                    } catch (ex: IOException) {
                        Log.e(TAG, "excption: ", ex)
                        return@request
                    }
                } else {
                    Log.e(TAG,"Failed to copy pixels: $copyResult")
                }
                handlerThread.quitSafely()
            }, Handler(handlerThread.looper))
        }
    }

    fun saveImage(bitmap: Bitmap, imageFile: File) {
        try {
            val outputStream  = FileOutputStream(imageFile)
            val outputData = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputData)
            outputData.writeTo(outputStream)
            outputStream.flush()
            outputStream.close()
        } catch (ex: IOException) {
            throw IOException("Failed to save bitmap", ex)
        }
    }

}
