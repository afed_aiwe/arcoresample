package com.example.ar_stabilization.imageconverters

import android.graphics.Bitmap
import android.media.Image


interface ImageToBitmapConverter {

    fun convert(image: Image): Bitmap

}