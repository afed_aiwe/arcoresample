package com.example.ar_stabilization.imageconverters

import android.graphics.*
import android.media.Image
import com.example.ar_stabilization.extensions.YUV420TONV21
import com.example.ar_stabilization.imageconverters.ImageToBitmapConverter
import java.io.ByteArrayOutputStream


class Nv21ToJpegConverter :
    ImageToBitmapConverter {

    override fun convert(image: Image): Bitmap {
        val baOutputStream = ByteArrayOutputStream()
        val width = image.width
        val height = image.height
        val yuvImage: YuvImage = YuvImage(image.YUV420TONV21(), ImageFormat.NV21, width, height, null)
        yuvImage.compressToJpeg(Rect(0, 0, width, height), 100, baOutputStream)
        val byteForBitmap = baOutputStream.toByteArray()
        baOutputStream.close()
        val bitmap = BitmapFactory.decodeByteArray(byteForBitmap, 0, byteForBitmap.size)
        return bitmap
    }

}