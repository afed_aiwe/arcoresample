package com.example.arcoresample.ui

import android.app.AlertDialog
import android.content.Context
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import com.example.arcoresample.R

class DialogHelper(val context: Context) {

    private var alertDialog: AlertDialog? = null

    fun hideAlertDialog() {
        if (alertDialog?.isShowing == true) {
            alertDialog?.dismiss()
        }
    }

    fun showProgressDialog() {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Loading...")
        val progressBar = ProgressBar(context)
        val lp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        progressBar.layoutParams = lp
        builder.setView(progressBar)
        alertDialog = builder.create()
        alertDialog?.show()
    }

    fun showParamsDialog(view: View) {
        val builder = AlertDialog.Builder(context)
        builder.setView(view)
        builder.setCancelable(true)
        builder.setPositiveButton("Close") {_, _ -> }
        alertDialog = builder.create()
        alertDialog?.show()
    }
}