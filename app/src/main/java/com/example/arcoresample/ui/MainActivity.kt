package com.example.arcoresample.ui

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.graphics.PointF
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.util.Size
import android.view.Gravity
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.arcoresample.R
import com.example.ar_stabilization.extensions.setupHighestImageResolution
import com.example.ar_stabilization.fragment.WritingArFragment
import com.example.ar_stabilization.helpers.PhotoHelper
import com.example.ar_stabilization.helpers.PositionHelper
import com.example.ar_stabilization.helpers.StabilizationHelper
import com.example.ar_stabilization.misc.ModelLoader
import com.example.arcoresample.extensions.setHeight
import com.example.arcoresample.extensions.setWidth
import com.google.ar.core.Anchor
import com.google.ar.core.Point
import com.google.ar.sceneform.AnchorNode
import com.google.ar.sceneform.rendering.ModelRenderable
import com.google.ar.sceneform.ux.TransformableNode
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File


class MainActivity : AppCompatActivity(),
    ModelLoader.ModelLoaderCallbacks {

    init {
        System.loadLibrary("opencv_java3")
    }

    companion object {
        private const val TAG = "MainActivity"
        private const val MIN_OPENGL_VERSION = 3.0

        private const val ENDPOINT_PHOTO_IN_DIRECTORY = "/Sceneform"
        private const val EXTENSION_IMAGE = ".jpg"

        const val IMAGE_COUNT = 10
        const val DIRECTORY_NAME = "DIRECTORY_NAME"
        const val SNAPSHOT_NAME = "snapshot"
    }

    lateinit var photoDirectory: File

    lateinit var directoryFolder: String

    private lateinit var arFragment: WritingArFragment

    private lateinit var modelLoader: ModelLoader

    private var sphereRenderable: ModelRenderable? = null

    private var currentAnchor: Anchor? = null

    private var currentAnchorNode: AnchorNode? = null

    private var hasSupportingCenterPosition = false

    private lateinit var cameraSize: Size

    private lateinit var dialogHelper: DialogHelper

    private var currentImage = 0

    private val incorrectPositionListener = object : PositionHelper.IncorrectPositionListener {
        override fun onIncorrectPosition(objectPosition: PointF) {
            dialogHelper.hideAlertDialog()
            showToast("Incorrect object position x = ${objectPosition.x} y = ${objectPosition.y}")
        }

        override fun onCorrectPosition(objectPosition: PointF) {
            takePhoto(objectPosition)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!checkIsSupportedDeviceOrFinish()) {
            return
        }

        setContentView(R.layout.activity_main)
        buildFile()
        dialogHelper = DialogHelper(this)
        arFragment = supportFragmentManager.findFragmentById(R.id.fragment) as WritingArFragment
        arFragment.setOnSessionInitializationListener { session ->
            cameraSize = session.setupHighestImageResolution()
            setupFragmentContainer()
        }
        modelLoader = ModelLoader(this)
        modelLoader.loadModel(this)

        val scene = arFragment.arSceneView.scene

        scene.addOnPeekTouchListener { hitTestResult, motionEvent ->
            if (sphereRenderable != null) {
                val frame = arFragment.arSceneView.arFrame
                val hitRes = frame?.hitTest(motionEvent)
                if (!hitRes.isNullOrEmpty()) {
                    for (hit in hitRes) {
                        val trackable = hit.trackable
                        if (trackable is Point) {
                            val anchor = hit.createAnchor()
                            val anchorNode = AnchorNode(anchor)
                            anchorNode.setParent(arFragment.arSceneView.scene)

                            clearAnchor()
                            currentAnchor = anchor
                            currentAnchorNode = anchorNode
                            createObjectRenderable(anchorNode)
                            hasSupportingCenterPosition = StabilizationHelper.getInstance().getSupportingPositionForStabilization(arFragment.arSceneView, cameraSize)
                            if (hasSupportingCenterPosition) {
                                showToast("Support center position taken")
                            }
                        }
                    }
                }
            }
        }

        button.setOnClickListener {
            if (currentImage < IMAGE_COUNT) {
                if (hasSupportingCenterPosition) {
                    checkBeforeTakePhoto()
                } else {
                    showToast("Object is not found on screen in center")
                }
            } else {
                showToast("All photos are taken")
                showPreviewActivity()
            }
        }
    }

    private fun createObjectRenderable(anchorNode: AnchorNode) {
        val objectCircle = TransformableNode(arFragment.transformationSystem)
        objectCircle.setParent(anchorNode)
        objectCircle.renderable = sphereRenderable
        arFragment.arSceneView.scene.addChild(anchorNode)
        objectCircle.select()
    }

    private fun clearAnchor() {
        currentAnchor = null

        if (currentAnchorNode != null) {
            arFragment.arSceneView.scene.removeChild(currentAnchorNode)
            currentAnchorNode?.anchor?.detach()
            currentAnchorNode?.setParent(null)
            currentAnchorNode = null
        }
    }

    override fun onResume() {
        super.onResume()
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        actionBar?.hide()
        PositionHelper.getInstance().addIncorrectPositionListener(incorrectPositionListener)
    }

    override fun onPause() {
        PositionHelper.getInstance().removeIncorrectPositionListener(incorrectPositionListener)
        super.onPause()
    }

    fun checkIsSupportedDeviceOrFinish(): Boolean {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            showToast("Sceneform requires Android N or later")
            finish()
            return false
        }
        val openGlVersionString = (getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager)
            .deviceConfigurationInfo
            .glEsVersion
        if (openGlVersionString.toDouble() < MIN_OPENGL_VERSION) {
            showToast("Sceneform requires OpenGL ES 3.0 later")
            finish()
            return false
        }
        return true
    }

    private fun showToast(message: String) {
        val toast = Toast.makeText(this, message,Toast.LENGTH_LONG)
        toast.setGravity(Gravity.CENTER_HORIZONTAL or Gravity.TOP, 0, 0)
        toast.show()
    }

    override fun setRenderable(modelRenderable: ModelRenderable?) {
        sphereRenderable = modelRenderable
    }

    override fun onLoadException(throwable: Throwable?) {
        showToast("Unable to load circle renderable")
    }

    private fun checkBeforeTakePhoto() {
        if (!arFragment.hasWritePermission()) {
            showToast("Photo capturing requires the WRITE_EXTERNAL_STORAGE permission")
        } else {
            dialogHelper.showProgressDialog()
            PositionHelper.getInstance().checkPositionObject(arFragment.arSceneView, currentAnchorNode, cameraSize)
        }
    }

    private fun takePhoto(objectPosition: PointF) {
        val filePath = File(photoDirectory, "$currentImage$EXTENSION_IMAGE")
        val snapshotPath = File(photoDirectory, "$SNAPSHOT_NAME$currentImage$EXTENSION_IMAGE")
        PhotoHelper.getInstance().takePhoto(arFragment.arSceneView, filePath, callback = {
            savePhotoParametrs(objectPosition)
        }, snapshotImageFile = snapshotPath)
        dialogHelper.hideAlertDialog()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        currentImage = 0
        button.text = "Take photo"
    }

    private fun savePhotoParametrs(objectPosition: PointF) {
        if (currentAnchorNode != null && arFragment.arSceneView.arFrame != null) {
            val objectPose = currentAnchor?.pose
            objectPose?.let {pose ->
                StabilizationHelper.getInstance().calculateParamsForCurrentFrame(pose, arFragment.arSceneView, objectPosition, "$currentImage", cameraSize)
            }
            currentImage++
            val countOfRemainingPhoto = IMAGE_COUNT - currentImage
            if (countOfRemainingPhoto == 0) {
                button.text = "Show preview"
                showToast("Press Show Preview")
            } else {
                showToast("Photo taken. Still left = ${(IMAGE_COUNT) - currentImage}")
            }
        }
    }

    private fun setupFragmentContainer() {
        fragmentContainer.setWidth(cameraSize.width)
        fragmentContainer.setHeight(cameraSize.height)
    }

    private fun buildFile() {
        directoryFolder = "${getDirectoryPictures()}$ENDPOINT_PHOTO_IN_DIRECTORY"
        photoDirectory = File(directoryFolder)

        val dir = photoDirectory
        if (!dir.exists()) {
            dir.mkdirs()
        }
    }

    private fun getDirectoryPictures(): String = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES)?.path ?: this.filesDir.path

    private fun showPreviewActivity() {
        val intent = Intent(this, PreviewActivity::class.java)
        intent.putExtra(DIRECTORY_NAME, directoryFolder)
        startActivityForResult(intent, 1)
    }
}
