package com.example.arcoresample.ui

import android.app.Activity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.ar_stabilization.helpers.StabilizationHelper
import com.example.arcoresample.R
import com.example.arcoresample.ui.MainActivity.Companion.DIRECTORY_NAME
import kotlinx.android.synthetic.main.activity_preview.*
import kotlinx.android.synthetic.main.dialog_view.view.*
import java.io.File
import java.util.regex.Pattern


class PreviewActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "PreviewActivity"
        private const val NEXT_IMAGE = 0
        private const val PREVIOUS_IMAGE = 1
    }

    private lateinit var directoryFolder: String

    private var listOfPhotoPath = mutableListOf<String>()

    private var startX: Float = 0.toFloat()
    private var startY: Float = 0.toFloat()
    private var tmpPoint = 0f

    private var position = 0

    private val pattern = Pattern.compile("\\d+")
    private val stabilizationPattern = Pattern.compile("\\d+_stabilization")

    private lateinit var dialogHelper: DialogHelper

    private val incorrectRotateListener = object : StabilizationHelper.IncorrectRotateListener {
        override fun onIncorrectRotate(errorMessage: String) {
            photoText.text = errorMessage
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_preview)

        dialogHelper = DialogHelper(this)

        directoryFolder = intent.getStringExtra(DIRECTORY_NAME) ?: ""

        StabilizationHelper.getInstance().addIncorrectRotateListener(incorrectRotateListener)

        photoStabilize.setOnClickListener {
            dialogHelper.showProgressDialog()
            StabilizationHelper.getInstance().stabilizePhoto(directoryFolder)
            setupImageList()
            text.visibility = View.GONE
            photoStabilize.visibility = View.GONE
            photoText.visibility = View.VISIBLE
            dialogHelper.hideAlertDialog()
            setImageToImageView(position)
            setupImageListener()
        }

        showDialog.setOnClickListener {
            val dialogView = setupDialogView()
            dialogHelper.showParamsDialog(dialogView)
        }
    }

    private fun setupImageList() {
        File("$directoryFolder/").walkTopDown().forEach {
            val imagePath = it.absolutePath
            val photoName = imagePath.substringAfterLast("/")
            val matcher = stabilizationPattern.matcher(photoName)
            if (matcher.find()) {
                listOfPhotoPath.add(imagePath)
            }
        }
        listOfPhotoPath.sort()
    }

    private fun setupDialogView(): View {
        val supportingParams = StabilizationHelper.getInstance().getSupportingParamsForPhoto()
        val centerPoint = StabilizationHelper.getInstance().getSupportingCenterPosition()
        val dialogView = layoutInflater.inflate(R.layout.dialog_view, null)
        dialogView.centerView.text = "Center\nx=${centerPoint.x}\ny=${centerPoint.y}"
        dialogView.yawView.text = "Yaw\n${supportingParams?.yaw?.toDouble()?.let { Math.toDegrees(it).toFloat()}}"
        dialogView.distanceView.text = "Distance\n${supportingParams?.distance}"

        val originalPhotoName = getOriginalPhotoName(position)
        if (originalPhotoName.isNotEmpty()) {
            val currentFrameParams = StabilizationHelper.getInstance().getCurrentFrameParams(
                originalPhotoName
            )
            dialogView.centerFrame.text =
                "Center\nx=${currentFrameParams?.positionObject?.x}\ny=${currentFrameParams?.positionObject?.y}"
            dialogView.yawFrame.text =
                "Yaw\n${currentFrameParams?.yaw?.toDouble()?.let { Math.toDegrees(it).toFloat() }}"
            dialogView.distanceFrame.text = "Distance\n${currentFrameParams?.distance}"
        }
        return dialogView
    }

    private fun setupImageListener() {
        previewImage.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View, event: MotionEvent): Boolean {
                val x = event.rawX
                val y = event.rawY
                val params = v.layoutParams as FrameLayout.LayoutParams
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        startX = x - params.leftMargin
                        startY = y - params.rightMargin
                        return true
                    }
                    MotionEvent.ACTION_MOVE -> {
                        val path = x - startX
                        if (path - tmpPoint > 75f)  {
                            changeImage(path, PREVIOUS_IMAGE)
                        }
                        if (path - tmpPoint < -75f) {
                            changeImage(path, NEXT_IMAGE)
                        }
                        return true
                    }
                    MotionEvent.ACTION_UP -> {
                        tmpPoint = 0f
                    }
                }
                return false
            }
        })
    }

    private fun changeImage(path: Float, state: Int) {
        tmpPoint = path
        when(state) {
            NEXT_IMAGE -> {
                position++
                if (position > listOfPhotoPath.size - 1) position = 0
            }
            PREVIOUS_IMAGE -> {
                position--
                if (position < 0) position = listOfPhotoPath.size - 1
            }
        }
        setImageToImageView(position)
    }

    private fun setImageToImageView(position: Int) {
        val imagePath = listOfPhotoPath[position]
        Glide.with(this)
            .load(File(imagePath))
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true)
            .into(previewImage)
        val imageName = imagePath.substringAfterLast("/")
        if (imageName == "Sceneform") {
            photoText.text = "Swipe screen"
        } else {
            photoText.text = imageName
        }
    }

    override fun onResume() {
        super.onResume()
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        actionBar?.hide()
    }

    override fun onDestroy() {
        StabilizationHelper.getInstance().removeIncorrectRotateListener(incorrectRotateListener)
        super.onDestroy()
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    private fun getOriginalPhotoName(position: Int): String {
        val photoPath = listOfPhotoPath[position]
        var originalPhotoName = ""
        val fullPhotoName =  photoPath.substringAfterLast("/")
        val matcher = pattern.matcher(fullPhotoName)
        if (matcher.find()) {
            originalPhotoName = matcher.group(0) ?: ""
        }
        return originalPhotoName
    }
}
