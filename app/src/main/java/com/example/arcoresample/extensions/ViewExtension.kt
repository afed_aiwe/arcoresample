package com.example.arcoresample.extensions

import android.view.View

fun View.setWidth(width: Int) {
    layoutParams?.let { params ->
        params.width = width
        layoutParams = params
    }
}

fun View.setHeight(height: Int) {
    layoutParams?.let { params ->
        params.height = height
        layoutParams = params
    }
}